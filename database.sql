-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 13, 2020 at 01:05 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projekat1`
--

-- --------------------------------------------------------

--
-- Table structure for table `korisnik`
--

CREATE TABLE `korisnik` (
  `ID_KORISNIKA` int(11) NOT NULL,
  `ID_RADNOG_MJESTA` int(11) NOT NULL,
  `ID_ROLE` int(11) NOT NULL,
  `KORISNICKA_LOZINKA` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `IME` varchar(15) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PREZIME` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EMAIL` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `DATUM_ZASNIVANJA_RADNOG_ODNOSA` date NOT NULL,
  `GODINE_RADNOG_STAZA` decimal(2,0) NOT NULL,
  `ID_ORGANIZACIONE_JEDINICE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `korisnik`
--

INSERT INTO `korisnik` (`ID_KORISNIKA`, `ID_RADNOG_MJESTA`, `ID_ROLE`, `KORISNICKA_LOZINKA`, `IME`, `PREZIME`, `EMAIL`, `DATUM_ZASNIVANJA_RADNOG_ODNOSA`, `GODINE_RADNOG_STAZA`, `ID_ORGANIZACIONE_JEDINICE`) VALUES
(20, 102, 2, '$2y$10$JsYfKVBRlaU1g9IVXRODPusewzr/DfBoiyYIVEFkcSDlA/5P8YYQC', 'Natasa', 'Jugovic', 'natasa.jugovic@walter.ba', '2018-09-27', '16', 0),
(30, 3, 2, '$2y$10$8MSnoAXPc2Gb4QnrqOAgveoh/WsZcRZtHksJ4gFsTZXHiduBJuUja', 'Dragana', 'Cajic', 'dragana.cajic@walter.ba', '2018-08-15', '16', 0),
(36, 102, 3, '$2y$10$3jwa8SUkCEHzCn8/umUmZ.l2jP44FtwZWpvYpHq2I1rSZ6N/WcCGy', 'Pavle', 'Poljčić', 'pavlepoljcic@gmail.com', '2018-09-28', '13', 0),
(64, 103, 2, '$2y$10$0jvGTc4wgx2yS0.h/s0Eqe55oNrSmiHTsR5OaKPqTHPk6tWHCRJ7S', 'sonja', 'poljcic', 'sonjapoljcic@gmail.com', '2020-02-20', '6', 1),
(139, 102, 1, '$2y$10$nnmhFpj0VqVyk3F.LA8zieI0twcj5DDg5lfXBKCr6s5G4fsiucbKu', 'pavle', 'poljcic', 'pavle_poljcic@yahoo.com', '2020-03-24', '6', 1);

-- --------------------------------------------------------

--
-- Table structure for table `organizaciona_jedinica`
--

CREATE TABLE `organizaciona_jedinica` (
  `ID_ORGANIZACIONE_JEDINICE` int(11) NOT NULL,
  `NAZIV_ORGANIZACIONE_JEDINICE` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organizaciona_jedinica`
--

INSERT INTO `organizaciona_jedinica` (`ID_ORGANIZACIONE_JEDINICE`, `NAZIV_ORGANIZACIONE_JEDINICE`) VALUES
(1, 'ETF'),
(3, 'Firma1'),
(5, 'Apoteka'),
(32, 'org1'),
(47, 'eee'),
(48, 'yyy');

-- --------------------------------------------------------

--
-- Table structure for table `radno_mjesto`
--

CREATE TABLE `radno_mjesto` (
  `ID_RADNOG_MJESTA` int(11) NOT NULL,
  `NAZIV_RADNOG_MJESTA` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ID_ORGANIZACIONE_JEDINICE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `radno_mjesto`
--

INSERT INTO `radno_mjesto` (`ID_RADNOG_MJESTA`, `NAZIV_RADNOG_MJESTA`, `ID_ORGANIZACIONE_JEDINICE`) VALUES
(102, 'Inženjer softvera', 1),
(103, 'Menadžer', 1),
(104, 'Dizajner zvuka', 1),
(105, 'Developer', 1),
(107, 'radnik1', 3),
(108, 'radnik2', 3),
(109, 'radnik3', 3),
(111, 'radnik5', 3),
(112, 'radnik6', 3);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `ID_ROLE` int(11) NOT NULL,
  `TIP_ROLE` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`ID_ROLE`, `TIP_ROLE`) VALUES
(1, 'Admin'),
(2, 'Radnik'),
(3, 'Glavni_admin');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `STATUS_ID` int(11) NOT NULL,
  `OPIS` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`STATUS_ID`, `OPIS`) VALUES
(1, 'Podnesen'),
(2, 'Odbijen'),
(3, 'Iskorišćen'),
(4, 'Odobren');

-- --------------------------------------------------------

--
-- Table structure for table `zahtjev`
--

CREATE TABLE `zahtjev` (
  `ID_ZAHTJEVA` int(11) NOT NULL,
  `ID_KORISNIKA` int(11) NOT NULL,
  `ID_ADMINA` int(11) DEFAULT NULL,
  `STATUS_ID` int(11) NOT NULL,
  `DATUM_POCETKA_GODISNJEG` date NOT NULL,
  `DATUM_POVRATKA` date NOT NULL,
  `KOMENTAR` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `GODINA` decimal(4,0) NOT NULL,
  `BROJ_DANA` decimal(2,0) NOT NULL,
  `BONUS_DANI` decimal(2,0) NOT NULL,
  `PRVI_DIO_ODMORA` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zahtjev`
--

INSERT INTO `zahtjev` (`ID_ZAHTJEVA`, `ID_KORISNIKA`, `ID_ADMINA`, `STATUS_ID`, `DATUM_POCETKA_GODISNJEG`, `DATUM_POVRATKA`, `KOMENTAR`, `GODINA`, `BROJ_DANA`, `BONUS_DANI`, `PRVI_DIO_ODMORA`) VALUES
(208, 30, 30, 4, '2018-11-08', '2018-11-22', '', '2018', '10', '4', 1),
(209, 30, 30, 4, '2019-02-26', '2019-03-07', '', '2019', '1', '4', 0),
(210, 30, 30, 4, '2019-02-27', '2019-02-28', '', '2019', '0', '4', 0),
(211, 20, 20, 4, '2019-02-27', '2019-03-14', '', '2019', '9', '4', 1),
(212, 20, 20, 4, '2019-02-26', '2019-02-28', '', '2019', '7', '4', 0),
(221, 36, 36, 4, '2020-02-19', '2020-03-04', '', '2020', '9', '3', 1),
(255, 36, 36, 4, '2020-02-25', '2020-02-26', '', '2020', '5', '3', 0),
(257, 64, 64, 1, '2020-02-25', '2020-03-06', NULL, '2020', '21', '1', 1),
(279, 64, 64, 3, '2020-02-27', '2020-02-28', '', '2020', '19', '1', 1),
(309, 64, 64, 1, '2020-03-09', '2020-03-12', NULL, '2020', '19', '1', 0),
(310, 64, 64, 4, '2020-03-09', '2020-03-11', '', '2020', '17', '1', 0),
(311, 64, 64, 1, '2020-03-10', '2020-03-12', NULL, '2020', '19', '1', 0),
(312, 64, 64, 1, '2020-03-09', '2020-03-12', NULL, '2020', '17', '1', 0),
(313, 36, 36, 4, '2020-03-09', '2020-03-11', '', '2020', '4', '3', 0),
(316, 64, 64, 4, '2020-03-10', '2020-03-11', '', '2020', '18', '1', 0),
(320, 64, 64, 4, '2020-03-17', '2020-03-19', '', '2020', '16', '1', 0),
(321, 64, 64, 4, '2020-03-17', '2020-03-19', '', '2020', '16', '1', 0),
(322, 64, 64, 4, '2020-03-17', '2020-03-19', '', '2020', '16', '1', 0),
(330, 139, 139, 4, '2020-03-23', '2020-04-03', '', '2020', '11', '1', 1),
(332, 139, 139, 1, '2020-04-01', '2020-04-03', NULL, '2020', '11', '1', 0),
(352, 36, 36, 1, '2020-04-06', '2020-04-08', NULL, '2020', '4', '3', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `korisnik`
--
ALTER TABLE `korisnik`
  ADD PRIMARY KEY (`ID_KORISNIKA`),
  ADD KEY `FK_EVIDENCIJA` (`ID_ROLE`),
  ADD KEY `FK_RADI` (`ID_RADNOG_MJESTA`),
  ADD KEY `FK_ORGANIZACIJA` (`ID_ORGANIZACIONE_JEDINICE`) USING BTREE;

--
-- Indexes for table `organizaciona_jedinica`
--
ALTER TABLE `organizaciona_jedinica`
  ADD PRIMARY KEY (`ID_ORGANIZACIONE_JEDINICE`),
  ADD KEY `ID_organizacione_jedinice` (`ID_ORGANIZACIONE_JEDINICE`);

--
-- Indexes for table `radno_mjesto`
--
ALTER TABLE `radno_mjesto`
  ADD PRIMARY KEY (`ID_RADNOG_MJESTA`),
  ADD KEY `FK_posjeduje` (`ID_ORGANIZACIONE_JEDINICE`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`ID_ROLE`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`STATUS_ID`);

--
-- Indexes for table `zahtjev`
--
ALTER TABLE `zahtjev`
  ADD PRIMARY KEY (`ID_ZAHTJEVA`),
  ADD KEY `FK_KREIRANJE_ZAHTJEVA` (`ID_KORISNIKA`),
  ADD KEY `FK_AZURIRANJE_ZAHTJEVA` (`ID_ADMINA`),
  ADD KEY `FK_STANJE` (`STATUS_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `korisnik`
--
ALTER TABLE `korisnik`
  MODIFY `ID_KORISNIKA` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140;

--
-- AUTO_INCREMENT for table `organizaciona_jedinica`
--
ALTER TABLE `organizaciona_jedinica`
  MODIFY `ID_ORGANIZACIONE_JEDINICE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `radno_mjesto`
--
ALTER TABLE `radno_mjesto`
  MODIFY `ID_RADNOG_MJESTA` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `ID_ROLE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `STATUS_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `zahtjev`
--
ALTER TABLE `zahtjev`
  MODIFY `ID_ZAHTJEVA` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=353;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
